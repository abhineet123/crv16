%!TEX ROOT = modularDecomposition.tex

\section{Descriptions of submodules}
A registration based tracker can be decomposed into three sub modules: appearance model (\textbf{AM}),   state space model (\textbf{SSM}) and search method (\textbf{SM}). Figure \ref{flow} shows how these modules work together in a complete tracking system assuming there is no model update wherein the appearance model of the object is updated as tracking progresses.  

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=2.8in]{submodules.png}
	\caption{Modular breakdown of a registration based tracker assuming there is no dynamic update to the appearance model. This shows how different components work, as formulated in Eq \ref{reg_tracking}}
	\label{flow}
	\end{center}
\end{figure}


When a geometric transform  $\mathbf{w}$ with parameters $\mathbf{p}=(p_1, p_2, ..., p_S)$ is applied to an image patch $\mathbf{x}$, the transformed patch is denoted by $\mathbf{x}'=\mathbf{w}(\mathbf{x}, \mathbf{p})$ and the corresponding pixel values as $\mathbf{I}(\mathbf{w}(\mathbf{x}, \mathbf{p}))$. Tracking can then be formulated (Eq \ref{reg_tracking}) as a state prediction problem where we need to predict the optimal transform parameters $\mathbf{p_t}$ for an image $\mathbf{I_t}$ that 
maximize the similarity, measured by a suitable metric $f$, between the target patch $\mathbf{I^*} = \mathbf{I_0}(\mathbf{w}(\mathbf{x},\mathbf{p_0}))$
and the warped image patch $\mathbf{I_t}(\mathbf{w}(\mathbf{x},\mathbf{p_t}))$. 
%The size of the state vector $\mathbf{p}$ determines how accurately the target is to be followed with greater values of $S$ usually leading to higher precision tracking. 
%This is the state space where search is performed and the algorithm that minimizes Eq \ref{tracking} is the search method. 
\begin{equation}
\begin{aligned}
\label{reg_tracking}
\mathbf{p_t} = \underset{\mathbf{p}} {\mathrm{argmax}} ~f(\mathbf{I^*},\mathbf{I_t}(\mathbf{w}(\mathbf{x},\mathbf{p})))
\end{aligned}
\end{equation}
We refer to the similarity metric $ f $, the warp function $ \mathbf{w} $ and the algorithm that maximizes Eq \ref{tracking} respectively as AM, SSM and SM.
A more detailed description of these submodules follows.

\subsection{Search Method}
\label{searchMethod}
This is the optimization procedure that searches for the warped patch in the the current image that best matches the original template. Gradient descent is the most popular optimization approach employed in tracking due to its speed and simplicity and is the basis of the classic Lucas Kanade tracker \cite{lk}. This algorithm can be formulated in four different ways \cite{ic} depending on which image is searched for the warped patch - initial or current - and how the parameters of the warping function are updated in each iteration - additive or compositional. The four resulting variants - forward additive (\textbf{FALK}), inverse additive (\textbf{IALK}),  forward compositional (\textbf{FCLK}) and inverse compositional (\textbf{ICLK}) - were  analyzed mathematically and shown to be equivalent to first order terms in \cite{ic}. Here, however, we show experimental results proving that their performance on real video benchmarks is quite different (Sec. \ref{res_sm}). 

A relatively recent update to this approach was in the form of Efficient Second order Minimization (\textbf{ESM})\cite{esm} technique that tries to make the best of both inverse and forward formulations by using the mean of the initial and current Jacobians. 
%However, as far aw we know, only the compositional variant has so far been reported in literature. Since an additive formulation is equally plausible, we have also tested an additive variant of ESM (\textbf{AESM}).  
We would like to mention here that, even though the authors of \cite{esm} used $\mathcal{SL}3$ parameterization for their ESM formulation and gave theoretical proofs as to why it is essential for this SM, we have used standard parameterization (using matrix entries as in \cite{szeliski, ic}) for all our experiments since, as we show later (Sec. \ref{res_ssm}), ESM actually performs identically with several different parameterizations.

Nearest neighbor search (\textbf{NN}) is another SM that has recently been used for tracking \cite{nnic} thanks to the FLANN library\cite{flann} that makes real time searches feasible. However, this method tends to give jittery and unstable results when used by itself due to the very limited seach space and so is usually used in conjunction with a gradient descent method to create a composite tracker that performs better than either of its constituents. We have used ICLK as this second tracker due to its speed (\textbf{NNIC}). 

Further details about these methods can be found in the publications listed in Table \ref{search}. 

\begin{table}[h]
	\caption{Search Methods for Tracking}
	\label{search}
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|}
			\hline
			\bf{IALK}        & \bf{FALK}    & \bf{FCLK} & \bf{ICLK} & \bf{ESM} & \bf{NN}      &\bf{NNIC}  \\ \hline
			%				& & & \bf{$[2,4,6,8]$} \
			\cite{hager1998} & \cite{lk} & \cite{szeliski} & \cite{ic} & \cite{esm} & \cite{nnic}& \cite{nnic}\\
			\hline 
		\end{tabular}\\
	\end{center}	
\end{table}


\subsection{Appearance Model}
\label{appearanceModel}

This is the similarity metric defined by the function $f$ in Eq. \ref{reg_tracking} using which the SM compares different warped patches from the current image to get the closest match with the original template. 
%In other words, it is the measure of similarity that the SM uses to determine if a candidate patch is a good match for the template.

The sum of squared differences (\textbf{SSD}) or the L2 norm of pixel differences is the AM used most often used in literature especially with SMs based on gradient descent search due to its simplicity and the ease of computing its derivatives. However, the same simplicity also makes it vulnerable to providing false matches when the object's appearance changes due to factors like lighting variations, motion blur and occlusion. 

To address these issues, more robust AMs have been proposed including Mutual Information (\textbf{MI}), Cross Cumulative Residual Entropy (\textbf{CCRE}), Normalized Cross Correlation (\textbf{NCC})\cite{ncc2012} and Sum of Conditional Variance (\textbf{SCV})\cite{scv} all of which supposedly provide a degree of invariance to changes in illumination. There also exists a slightly different formulation of the latter known as the Reversed SCV (\textbf{RSCV}) \cite{nnic,hager} where the current image is updated rather than the template. In addition, there has been a recent extension to it called \textbf{LSCV} \cite{} that uses multiple joint histograms from corresponding sub regions within target patch to have more robustness to localized intensity changes. the Table \ref{appearance} lists the publications that can be referred for more details on these models.

It has further been shown in \cite{Ruthotto2010_thes_ncc_equivalence} that maximizing NCC between two images is equivalent to minimizing the SSD between two z-score \cite{Jain2005_ncc_z_score} normalized images. We consider the resultant formulation as a different AM called Zero Mean NCC (\textbf{ZNCC}).

\begin{table}[h]
	\caption{Appearance Models for Tracking}
	\label{tab_appearance}
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			\bf{SSD}&\bf{SCV}&\bf{RSCV}&\bf{LSCV}&\bf{ZNCC} &\bf{MI}&\bf{CCRE}&\bf{NCC}  \\ \hline
			\cite{ic,lk}&\cite{scv}&\cite{esm,nnic}&\cite{Santner2010_ncc}&\cite{esm,nnic} & \cite{ncc}& \cite{ncc}& \cite{ncc} \\
			\hline 
		\end{tabular}\\
	\end{center}	
\end{table}

\subsection{State Space Model}
\label{stateSpace}
The state space model \textbf{SSM} represents any constraints that are placed on the search space of warp parameters to make search more efficient. This includes both the DOF of allowed motion, as well as the actual parameterization of the warping function. For instance, the ESM tracker, as presented in \cite{esm}, can be considered to search over a different state space of warping parameters than conventional Lucas Kanade type trackers \cite{lk,ic} since it uses an $\mathcal{SL}3$ parameterization of homography rather than the actual entries of the corresponding matrix.

The advantage of allowing higher DOF in the warping function is achieving greater precision in the aligned warp since transforms that are higher up in the hierarchy \cite[sec. 2.4]{Hartley:2003:MVG:861369}  can better approximate the projective transformation process that captures the relative motion between the camera and the object in the 3D world into the 2D images. We model 5 different DOFs (2, 3, 4, 6, 8) according to \cite{szeliski}.

%The third sub module, namely SSM, represents any constraints that may be placed on the warping parameters to reduce the search space and make it easier for the SM to find the optimal warp.
It may be noted that this sub module differs from the other two since it does not admit new methods in the conventional sense and may even be viewed as a part of the SM with the two being often closely intertwined in practical implementations.
However, though the SSMs used in this work are limited to the standard hierarchy of geometric transformations with 2, 3, 4, 6 and 8 degrees of freedom (DOF) \cite{Hartley:2003:MVG:861369}, it is theoretically possible to impose novel constraints on the search space that can significantly decrease the search time while still producing sufficiently accurate results. The fact that such a constraint will be an important contribution in its own right justifies the use of SSM as a sub module in this work to motivate further research in this direction.
% However, there are two issues with having to estimate more free parameters. Firstly, the iterative search takes longer to converge making the tracker slower. Secondly, the search process becomes more likely to either diverge or end up in a local minimum thus causing the tracker to be less stable and more likely to lose track. The latter is a well known phenomenon with Lucas Kanade type trackers \cite{Bouguet01} whose higher DOF (e.g. affine) variants are much less robust than simple 2 DOF versions. Both section ~\ref{searchMethod} and ~\ref{stateSpace} are together refered to as the motion model. 